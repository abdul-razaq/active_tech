# About The Project

This README file contains detailed information about the Active Tech Application. How to run it, and the Technologies that are involved.

## Reproducing and Quick Run

You can clone this repository by running the following command on your terminal in a preferred directory like so:
`git clone https://gitlab.com/abdul-razaq/active_tech`
> You can run the following command to start the application without initializing a container first by running the following command.
> 
`go mod download && go mod verify`

> You can run the unit test itself to ensure that everything runs correctly even before running the application by running the following command:

`go test ./cmd/web/`

> To build and run the application in just one step, run the following command:

`go run ./cmd/web/`

> You can then access the application at the following address:

`http://localhost:4000/`


## Application Technologies

This section lists the technologies and tools used to build this application or the tools and technologies that this application depends on and how to run the application itself (in a container)

- **Golang**
	> Golang is the programming language used to develop this application. The web server that serves the content at the home route ("/") is developed in the Google Go Programming language.
	> It uses the built in Go standard library's http package to boostrap and start up a server.
	> It accepts only one optional runtime parameter ("addr") that defines the address and port that the server listens on. (e.g, ":5000").
	> The default address if no parameter is passed is ":4000" which simply means that the server will listen on all available network interface and on port 4000 by default.
	> The content at "/" is hard coded in the code base itself so the response is always static.
	> The application defines a custom logger that logs info and errors to the terminal by default.
	
- **Docker**
	> Docker is used to containerize this application, which means this application can run in any environment that has the Docker engine installed and the docker daemon running.
	> In the root directory of this project, a Dockerfile is present that defines or instructs the Docker engine how to create a docker image out of this file.
	> This Dockerfile uses the multi-stage build technique to significantly reduce the final size of the application build.
	> To create an image out of this Dockerfile and run a container from it on your local machine (provided that the Docker engine is already installed and the docker deamon is running), use the following command:
	
	`docker image build -t active_tech:v1.0 .`
	> Take note of the "*.*" after the command which defines the context (The directory where the Dockerfile is defined) which the Docker engine uses to build the image.
	> Once the image has been built, use the command:
	
	`docker images` or `docker image ls` to verify that the image has already been built successfully
	> Use the command to start a container out of the built image:
	
	`docker container run -p 5000:4000 active_tech:v1.0`
	or
	`docker container run -p 5000:5000 active_tech:v1.0 -addr=":5000"`
	
	> In the command above, we defined a custom address that specifies which address the server will listen on. In this case, we set the "addr" flag to ":5000"
	> Now, we can access the application's content at the "/" endpoint using:
	
	`http://localhost:4000/` or `http://localhost:5000` if you specify a custom "addr" flag like in the second command above.

## DevOps
In the root of this project directory, there is a file called **.gitlab-ci.yml**. This file indicates the presence of a CI/CD pipeline configuration. This project makes use of the **Gitlab CI/CD** tool to enable **Continuous Integration and Continuous Deployment** of the project, which means that this project follows the **DevOps principles**. As soon as a change is made to this project, for example, a developer adding a new feature or making changes to certain parts of the Application, the **CI/CD Pipeline** is triggered which in turn runs a series of **Jobs** in a few different **Stages**. These Jobs define the logical steps that the project must pass through before it reaches the **Consumer(s)** of the application. 

In this Pipeline configuration, the following Jobs are defined which are grouped by the stages that they belong to:

-	**Bootstrap**
	> This stage defines the Initial resource creation and management that must take place in order to allow smooth deployment of the project. For example, Gitlab Runners where the jobs will be executes, Kubernetes Cluster where the Application image will be deployed to and managed.
	-  **bootstrap_terraform_state_store**
	> This job is responsible for creating the storage that hosts the Terraform state files using AWS S3 bucket.
	-  **bootstrap_runners**
	> This job is responsible for creating the runners and the virtual servers where the jobs in our pipeline will run.
	- **create_kubernetes_cluster**
	> This job is responsible for creating the Kubernetes Cluster including the control plane and workload plane where our Application container image will run and be managed.

-	**Test**
	> This stage defines the Jobs responsible for running several different kinds of tests on the Application including SAST( Static Analysis Security Test). 
	-  **sast**
	> This job is responsible for running static analysis test on the project using a Gitlab Security Pipeline Template.
	-  **run_unit_tests**
	> This job is responsible for running the unit tests on the project.
	-  **run_unit_tests**
	> This job is responsible for running the unit tests on the project.

-	**Build**
	> This stage defines the Jobs responsible for building the Application. For example building the Application into a Container Image and pushing the Container Image into a remote Container Registry e.g The Gitlab Container Registry.
	-  **build_image**
	> This job is responsible for building the Application into a Docker Container Image artifact.
	-  **push_image**
	> This job is responsible for pushing the built Application to the Gitlab Container Registry.

-	**Deploy**
	> This stage defines the Jobs responsible for deploying the built Application into the Kubernetes Cluster created in the previous "create_kubernetes_cluster" job in the Bootstrap stage, and also ensures to deploy into different environments.
	-  **deploy_to_dev**
	> This job is responsible for deploying the Container Image into the Kubernetes Cluster in the Dev Environment
	-  **deploy_to_staging**
	> This job is responsible for deploying the Container Image into the Kubernetes Cluster in the Staging Environment
	-  **deploy_to_prod**
	> This job is responsible for deploying the Container Image into the Kubernetes Cluster in the Production Environment.

*Best practices were used or applied in the creation of the pipeline code including using caches to speed up pipeline execution*

### DevOps Tools Used
- ### Gitlab CI/CD:
> Used to create the pipeline configuration.
- ### Terraform:
> Used to create and manage the cloud infrastructure resources including the Kubernetes Cluster used to run the Application container image and also the Virtual Servers used to host the Gitlab Runners responsible for running the Jobs in the Pipeline configuration
- ### AWS:
> The cloud platform where all the resources are hosted
- ### Docker:
> Used to create a container image for the Application
- ### Gitlab Container Registry:
> Used to host the built container image.
- ### Kubernetes
> Used to host, run and manage the Application.

### Steps to replicating the DevOps flow

In order to run the pipeline for this project, you need to have the following:

-  A Gitlab Account
-  An AWS Account

1. Create an AWS account then Sign in as the Root user (You can also enable MFA for the Root user for additional security) 
2. Create an IAM user with Admin Policies attached so that this user has the right privileges and permissions to create and manage resources on AWS from the Jobs in the Pipeline.
3. Create an Access Key for this user so that this credentials can be used to programatically access the AWS API from the Pipeline and create resources as this user.
4. Copy The AWS_ACCESS_KEY_ID and the AWS_SECRET_ACCESS_KEY credentials.
5. Create a Gitlab Account where this Application code base lives and clone this project into a repository on this Account.
6. Go to the CI/CD Settings section of the project repository and configure the following variables that are required in order to run this pipeline.
	- AWS_ACCESS_KEY_ID: The AWS Access Key ID
	- AWS_SECRET_ACCESS_KEY: The AWS Secret Access Key
	- GITLAB_RUNNER_REGISTRATION_TOKEN: The gitlab runner registration token that can be found in the "Runners" section of the CI/CD settings page that allows gitlab runners responsible for running jobs to be created on Virtual Server Instances in AWS.
7. Go to the project page and click the play icon to run the pipeline or make some changes to the code base and make a commit then push to the main branch to trigger the pipeline.
8. If all things are configured properly, the pipeline should run successfully and the "Environment" section of the project hosts the URL Endpoint of the Dev, Stage and Prod environment from which the Application can be accessed.
9. If after the Pipeline has executed successfully and the Gitlab environment section does not have the URLs (Which may occur due to a well known Gitlab bug), you can find the URL endpoint to the Application in the output of the respective Jobs by clicking on the Job itself (e.g on the "deploy_prod" job). The endpoint is located in a variable named "service_endpoint" which contains the public endpoint to access the application.

### Side Note

The code base for the infrastructure definition lives in a separate public repository in https://gitlab.com/abdul-razaq/active_tech_infrastructure to allow proper separation of concern and to decouple the Infrastructure code base from the Application code base.

## Network Constraints

While the pipeline runs successfully 90% of the time, A bad network can cause some Jobs to fail, especially Jobs that creates and manages the infrastructure using Terraform. This is because the state file that Terraform uses to manage the infrastructure is stored and fetched in a remote location (i.e In an S3 bucket). Terraform needs to remotely fetch this state file from the store while trying to create and manage the infrastructure. So if the network is bad, this can lead to Terraform throwing an error like: "Unable to release lock file". 

The solution is to retry the failed Job by clicking on the "Run again" icon in the failed Job which eventually causes the Job to pass.

*Thank you for reading this far...*
*I love DevOps and Automation*

### Built By
**AbdulRazaq Suleiman Ayomide** as part of an **Active Tech** Interview assessment for the **DevOps Engineer position.**