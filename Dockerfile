FROM golang:1.18 AS initial_stage

WORKDIR /app

COPY go.mod ./

RUN go mod download

COPY cmd/* internals/* ./

RUN CGO_ENABLED=0 GOOS=linux go build -o /bin/app

FROM initial_stage AS run_stage
# Run go test

FROM alpine:3.18.4 AS final_stage

WORKDIR /

COPY --from=initial_stage /bin/app /bin/app

EXPOSE 4000

ENTRYPOINT [ "./bin/app" ]