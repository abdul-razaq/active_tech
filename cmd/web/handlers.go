package main

import (
	"encoding/json"
	"net/http"
	"time"
)

type Data struct {
	Timestamp string `json:"timestamp"`
	Message   string `json:"message"`
}

func home(app *application) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		data := &Data{Timestamp: time.Now().Format(time.RFC850), Message: "Automate all the things!"}
		d, err := json.Marshal(data)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			app.errorLog.Println(err)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(d))
	})
}
