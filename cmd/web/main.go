package main

import (
	"flag"
	"log"
	"net/http"
	"os"
)

type application struct {
	infoLog  *log.Logger
	errorLog *log.Logger
}

var infoLog = log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
var errorLog = log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

var app = &application{
	infoLog:  infoLog,
	errorLog: errorLog,
}

func main() {
	addr := flag.String("addr", ":4000", "The network address the server will listen on")

	flag.Parse()

	mux := http.NewServeMux()

	mux.Handle("/", home(app))

	server := &http.Server{
		Addr:     *addr,
		Handler:  mux,
		ErrorLog: errorLog,
	}

	infoLog.Printf("Server listening on %s", *addr)
	err := server.ListenAndServe()
	errorLog.Fatalln(err)
}
